{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Definition eigener Datentypen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In vielen Programmiersprachen gibt es das Konzept der *Record-Datentypen*.\n",
    "Dies sind Datenstrukturen, die dafür gedacht sind, Datensätze mit mehreren Einträgen\n",
    "(auch von unterschiedlichen Typen) zu verwalten.\n",
    "\n",
    "In objektorientierten Sprachen wie C++ oder Java gibt es das darüber hinausgehende Konzept der *Klassen*. Dies sind gewissermaßen Record-Datentypen, die zusätzlich zu ihren Datenfeldern auch noch Funktionen mitbringen, die die inneren Werte manipulieren können.\n",
    "\n",
    "In diesem Notebook gehen wir anhand eines Beispiels auf die Grundlagen von Record-Datentypen\n",
    "ein. Die tiefergehenden Konzepte von Klassen und Vererbung werden in den folgenden Notebooks behandelt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Include-Anweisungen für die Beispiele aus diesem Notebook\n",
    "#include<string>\n",
    "#include<iostream>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Record-Datentypen (`struct`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein Beispiel für einen einfachen Record-Datentyp ist der Datensatz eines Spielers in einem einfachen Spiel. Ein solcher Datensatz kann z.B. den Namen und den Punktestand eines Spielers enthalten. In C++ könnte man so einen Datensatz folgendermaßen implementieren:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Player {\n",
    "    std::string name;\n",
    "    int score;\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Diese Definition erlaubt es uns, eine Variable vom Typ Spieler zu erzeugen, ihm Werte für seinen Namen und dem Punktestand zuzuweisen und diese Werte auch wieder auszulesen.\n",
    "Wir erzeugen beispielhaft zwei Spieler-Variablen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Player p1;\n",
    "Player p2{\"Arno Nym\", 42};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Beide Zeilen erzeugen jeweils eine Spieler-Variable. Der Spieler `p1` bleibt dabei unitialisiert, während die Einträge von `p2` die Werte aus den geschweiften Klammern bekommen.\n",
    "\n",
    "Man kann nun auf folgende Weise auf die Werte der Variablen zugreifen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p1.name = \"John Doe\";\n",
    "p1.score = -13;\n",
    "std::cout << \"Spielstand von \" << p2.name << \": \" << p2.score << \".\" << std::endl;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Anschaulich können wir uns vorstellen, dass die Deklaration `Player p1;` nicht nur eine, sondern zwei Variablen auf einmal erzeugt hat, nämlich `p1.name` und `p1.score`.\n",
    "Technisch gesehen ist es genau so, es wurde Speicher für zwei solche Variablen reserviert.\n",
    "Für die Praxis ist es allerdings meist besser, `p1` als eine Variable mit zwei sog. *Attributen* oder *Membern* zu betrachten.\n",
    "\n",
    "**Anmerkung zu den Begriffen:** Der Begriff *Attribut* ist im Kontext von Objektorientierung die gängige Bezeichnung für die Dateneinträge einer Klasse. Als *Member* werden typischerweise in C++ alle Bestandteile von Structs bzw. Klassen bezeichnet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Verwendungsbeispiel für den Player-Datentyp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir nutzen das oben definierte Struct nun, um ein kleines Spiel zu implementieren.\n",
    "Wir lassen zwei Spieler das Spiel \"Schere, Stein, Papier\" spielen.\n",
    "Zu den Spielregeln, siehe [Wikipedia](https://de.wikipedia.org/wiki/Schere,_Stein,_Papier).\n",
    "Die beiden Spieler sollen so lange spielen, bis einer drei Punkte hat."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zusätzlich zu den Spieler-Datenstrukturen benötigen wir noch eine Funktion `get_move()`,\n",
    "die einen Spieler auf der Konsole nach seiner Eingabe fragt.\n",
    "Wir legen hierfür fest, dass die Eingabe `1` für \"Schere\" stehen soll, die `2` für \"Stein\" und die `3` für Papier.\n",
    "Die Funktion `get_move()` erwartet einen `Player` als Parameter, damit sie ihn mit seinem Namen ansprechen kann.\n",
    "Der `Player` wird aus Performance-Gründen als *konstante Referenz* erwartet (vgl. [Notebook zu Funktionen](03_Funktionen.ipynb)).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int get_move(Player const & p)\n",
    "{\n",
    "    // Spieler zur Eingabe eines Zugs auffordern.\n",
    "    std::cout << p.name << \", Ihre moeglichen Zuege sind: \\n\"\n",
    "              << \"1: Schere, 2: Stein, 3: Papier\\n\\n\";\n",
    "    std::cout << \"Bitte geben Sie Ihren Zug ein und bestätigen Sie mit ENTER:\" << std::endl;\n",
    "    \n",
    "    // Eingabe einlesen und pruefen, wiederholen bis Erfolg.\n",
    "    bool valid = false;\n",
    "    std::string input;\n",
    "    while (!valid)\n",
    "    {\n",
    "        std::cin >> input;\n",
    "        valid = (input == \"1\" || input == \"2\" || input == \"3\");\n",
    "        if (!valid)\n",
    "        {\n",
    "            std::cout << \"\\nFehler, bitte versuchen Sie es noch einmal:\" << std::endl;\n",
    "        }\n",
    "    }\n",
    "    \n",
    "    // Eingabe in Zahl konvertieren und zurueckliefern.\n",
    "    // input[0] ist der ASCII-Code von '1', '2' oder '3'.\n",
    "    // Wir ziehen davon den ASCII-Code von '0' ab,\n",
    "    // um eine Zahl 1 <= x <= 3 zu erhalten.\n",
    "    return input[0] - '0';\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir probieren unsere Funktion kurz mit `p2` von oben aus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "get_move(p2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Eine einfache Implementierung ist nun die folgende Funktion `run()`:\n",
    "Sie erzeugt zwei Spieler (der Einfachheit halber mit Standard-Namen)\n",
    "und fragt diese abwechselnd nach ihren Zügen, bis einer von ihnen 3 Punkte hat."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void run()\n",
    "{\n",
    "    // Spieler erzeugen.\n",
    "    Player p1{\"Spieler 1\", 0};\n",
    "    Player p2{\"Spieler 2\", 0};\n",
    "    \n",
    "    // Spiel so lange laufen lassen, bis einer 3 Punkte hat.\n",
    "    while (p1.score < 3 && p2.score < 3)\n",
    "    {\n",
    "        // Spielzuege abfragen\n",
    "        int m1 = get_move(p1);\n",
    "        int m2 = get_move(p2);\n",
    "        \n",
    "        // Auswertung\n",
    "        if (m1 == m2) // Unentschieden\n",
    "        {\n",
    "            std::cout << \"Diese Runde endet unentschieden.\" << std::endl;\n",
    "        }\n",
    "        else if (m2 == m1+1 || m2 == m1-2) // Spieler 2 gewinnt.\n",
    "        {\n",
    "            std::cout << \"Spieler 2 gewinnt diese Runde.\" << std::endl;\n",
    "            p2.score++;\n",
    "        }\n",
    "        else\n",
    "        {\n",
    "            std::cout << \"Spieler 1 gewinnt diese Runde.\" << std::endl;\n",
    "            p1.score++;\n",
    "        }\n",
    "        std::cout << std::endl;\n",
    "    }\n",
    "    \n",
    "    // Ergebnis mitteilen\n",
    "    std::cout << (p1.score == 3 ? p1.name : p2.name) << \" hat gewonnen.\" << std::endl; \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wir probieren die `run()`-Funktion aus:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Damit haben wir die erste Version unseres Spiels fertig.\n",
    "Im Ordner `Codebeispiele/SchereSteinPapier` findet sich [diese Version](../Codebeispiele/SchereSteinPapier/ssp01.cpp) sowie die weiteren aus den Folge-Notebooks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "-std=c++17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
