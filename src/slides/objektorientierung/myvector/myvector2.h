class myvector
{
    int * m_array;
    int m_size;
    
public:
    // Konstruktor:
    myvector(int size)
    : m_array{new int[size]}
    , m_size{size}
    {}
    
    // Methoden zum Zugriff:
    int & at(int i) { return m_array[i]; }
    int size() { return m_size; }
};
