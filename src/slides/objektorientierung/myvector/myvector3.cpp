#include<iostream>
#include"myvector3.h"

int main() {
    // 10-stelligen 'vector' erzeugen:
    myvector vec(10);
    
    // 'vec' mit Zahlen füllen:
    for (int i=0; i<vec.size(); i++) {
        vec[i] = 2 * i;    
    }
    // 'vec' ausgeben:
    for (int i=0; i<vec.size(); i++) {
        std::cout << vec[i] << "\n";
    }
    return 0;
}
