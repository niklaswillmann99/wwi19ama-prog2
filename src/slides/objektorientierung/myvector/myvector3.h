class myvector {
    int * m_array;
    int m_size;
    
public:
    myvector(int size)
    : m_array{new int[size]}
    , m_size{size}
    {
      for (int i=0;i<m_size;i++)
      {
        m_array[i] = 0;
      }
    }
    
    // Methoden zum Zugriff:
    int & at(int i) { return m_array[i]; }
    int size() { return m_size; }
    
    // Operatoren:
    int & operator[](int i) { return at(i); }
    
    // Destruktor:
    ~myvector() { delete[] m_array; }
};
