#include "cell.h"

Cell::Cell()
: isWall(false)
, hasTreasure(false)
, hasPlayer(false)
{}

Cell::Cell(char c)
: isWall(c == '#')
, hasTreasure(c == 'T')
, hasPlayer(c == 'P')
{}
    
bool Cell::isBlocking()
{
    return isWall;
}

bool Cell::playerPresent()
{
    return hasPlayer;
}

bool Cell::treasurePresent()
{
    return hasTreasure;
}

bool Cell::empty()
{
    return !isBlocking() && !playerPresent() && !treasurePresent();
}

bool Cell::addPlayer()
{
    if (isWall) { return false; }
    hasPlayer = true;
    return true;
}

void Cell::removePlayer()
{
    hasPlayer = false;
}

bool Cell::addTreasure()
{
    if (isWall) { return false; }
    hasTreasure = true;
    return true;
}

void Cell::removeTreasure()
{
    hasTreasure = false;
}
    
Cell::operator char() const
{
    if (isWall) { return '#'; }
    if (hasPlayer) { return 'P'; }
    if (hasTreasure) { return 'T'; }
    return ' ';
}
