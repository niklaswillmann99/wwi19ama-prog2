#ifndef PLAYFIELD_H
#define PLAYFIELD_H

#include"cell.h"
#include<iostream>
#include<string>
#include<vector>
/**
 * Klasse Playfield
 *
 * Dient zum Verwalten eines Labyrinth-Spielfelds und bietet Funktionen zum Bewegen des Spielers,
 * zur Pruefung, ob Bewegungen moeglich sind und zum Nachverfolgen der gemachten Bewegungen.
 */
class Playfield
{
public:
    /// Konstruktor
    /// Erwartet einen Dateinamen als String und liest das Spielfeld aus dieser Datei.
    Playfield(std::string filename);

    /// Prueft, ob der Spieler auf das angegebene Feld bewegt werden kann.
    bool canMoveTo(int x, int y);

    /// Prueft, ob der Spieler um die angegebene Distanz in x- und y-Richtung bewegt werden kann.
    bool canMove(int dx, int dy);

    /// Prueft, ob der Spieler nach Norden bewegt werden kann.
    bool canMoveNorth();

    /// Prueft, ob der Spieler nach Osten bewegt werden kann.
    bool canMoveEast();

    /// Prueft, ob der Spieler nach Sueden bewegt werden kann.
    bool canMoveSouth();

    /// Prueft, ob der Spieler nach Westen bewegt werden kann.
    bool canMoveWest();

    /// Bewegt den Spieler auf das angegebene Feld, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
    bool moveTo(int x, int y);

    /// Bewegt den Spieler um dx und dy, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
    bool move(int dx, int dy);

    /// Bewegt den Spieler nach Norden, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
    bool moveNorth();

    /// Bewegt den Spieler nach Osten, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
	bool moveEast();

    /// Bewegt den Spieler nach Sueden, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
	bool moveSouth();

    /// Bewegt den Spieler nach Westen, wenn das moeglich ist.
    /// Liefert true, wenn die Bewegung durchgefuehrt wurde.
	bool moveWest();

	/// Liefert die aktuelle Position des Spielers als Paar aus int.
    std::pair<int,int> getPlayerPos() const;

    /// Liefert die aktuelle X-Position des Spielers.
    int getPlayerX() const;

    /// Liefert die aktuelle Y-Position des Spielers
    int getPlayerY() const;

    /// Liefert true, wenn der Spieler auf dem Schatz steht.
    bool treasureFound();

    /// Liefert eine String-Repraesentation des Spielfelds zurueck.
    std::string str() const;

    /// Liefert die Liste der Positionen zurueck, auf denen der Spieler sich bewegt hat.
    std::string getPositionHistory();

private:
    std::vector<std::vector<Cell>> grid;
    int xPos;
    int yPos;
    std::vector<std::pair<int,int>> positionHistory;

    /// Laedt das Spielfeld aus der angegebenen Datei.
    void loadFile(std::string fileName);

    /// Sucht die Markierung des Spielers und initialisiert damit die
    /// intern gespeicherte Position. Loescht die gespeicherte Historie.
    void initPlayerPos();
};

/// Stream-Ausgabe-Operator, damit ein Spilfeld einfach ausgegeben werden kann.
std::ostream & operator<<(std::ostream & lhs, Playfield const &);

#endif

