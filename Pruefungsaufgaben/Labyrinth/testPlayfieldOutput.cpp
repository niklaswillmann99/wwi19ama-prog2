#include"playfield.h"

#include<iostream>
using namespace std;

int main()
{
    vector<string> testFiles =
    {
        "tests/playfield01.txt",
        "tests/playfield02.txt",
        "tests/playfield03.txt",
        "tests/playfield04.txt",
        "tests/playfield05.txt",
        "tests/playfield06.txt"
    };
    
	for (auto fileName : testFiles)
    {
        Playfield p(fileName);
        cout << p << endl << endl;
    }
    
	return 0;
}
