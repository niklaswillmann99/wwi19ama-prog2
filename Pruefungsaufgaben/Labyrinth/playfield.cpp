#include"playfield.h"

#include<fstream>
#include<sstream>
using namespace std;

Playfield::Playfield(string fileName)
{
    loadFile(fileName);
    initPlayerPos();
}

bool Playfield::canMoveTo(int nx, int ny)
{
    return (ny >= 0 && ny < grid.size() &&
            nx >= 0 && nx < grid[ny].size() &&
            !grid[ny][nx].isBlocking());
}

bool Playfield::canMove(int dx, int dy)
{
    return canMoveTo(xPos + dx, yPos + dy);
}

bool Playfield::canMoveNorth()
{
    return canMove(0,-1);
}

bool Playfield::canMoveEast()
{
    return canMove(1,0);
}

bool Playfield::canMoveSouth()
{
    return canMove(0,1);
}

bool Playfield::canMoveWest()
{
    return canMove(-1,0);
}


bool Playfield::moveTo(int nx, int ny)
{
    // Wenn die neue Position noch auf dem Spielfeld
    // ist und das Setzen des Spielers gelingt,
    // entfernen wir den Spieler von der alten Position.
    if (canMoveTo(nx, ny) &&
        grid[ny][nx].addPlayer())
    {
        // Spieler von alter Position entfernen,
        // falls diese gueltig war.
        if (xPos >= 0 && yPos >= 0)
        {
            grid[yPos][xPos].removePlayer();
        }
        // Gespeicherte Spielerposition aktualisieren.
        xPos = nx;
        yPos = ny;
        positionHistory.emplace_back(xPos, yPos);
        return true;
    }
    return false;
}

bool Playfield::move(int dx, int dy)
{
    return moveTo(xPos + dx, yPos + dy);
}

bool Playfield::moveNorth()
{
    return move(0,-1);
}

bool Playfield::moveEast()
{
    return move(1,0);}

bool Playfield::moveSouth()
{
    return move(0,1);
}

bool Playfield::moveWest()
{
    return move(-1,0);
}

pair<int,int> Playfield::getPlayerPos() const
{
    return {xPos, yPos};
}

int Playfield::getPlayerX() const
{
    return xPos;
}

int Playfield::getPlayerY() const
{
    return yPos;
}

bool Playfield::treasureFound()
{
    return grid[yPos][xPos].treasurePresent();
}

std::string Playfield::str() const
{
	ostringstream out;
	for (auto row : grid)
	{
		for (Cell c : row)
        {
            out << c;
        }
        out << '\n';
	}
	return out.str();
}

string Playfield::getPositionHistory()
{
    ostringstream out;
    for (auto [x,y] : positionHistory)
    {
        out << "(" << x << "," << y << ")" << endl;
    }
    return out.str();
}

void Playfield::loadFile(string fileName)
{
    ifstream file(fileName);
    string line;
    
    while (getline(file, line))
    {
        grid.push_back({});
        for (char c : line)
        {
            grid.back().push_back(c);
        }
    }
    file.close();
}

void Playfield::initPlayerPos()
{
    xPos = -1;
    yPos = -1;
    positionHistory.clear();
    for (int y=0; y<grid.size(); y++)
    {
        for (int x=0; x<grid[y].size(); x++)
        {
            if (grid[y][x].playerPresent())
            {
                xPos = x;
                yPos = y;
                positionHistory.emplace_back(xPos, yPos);
                return;
            }
        }
    }
}

ostream & operator<<(ostream & lhs, Playfield const & rhs)
{
	return lhs << rhs.str();
}
