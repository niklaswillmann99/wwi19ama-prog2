#ifndef HEAP_H
#define HEAP_H

#include<vector>

class Heap
{
public:
    // Fuegt ein neues Element zum Heap hinzu.
    void add(int x);
    
    // Entfernt das Wurzelelement.
    void removeRoot();
    
    // Liefert den Wert des Wurzelelements.
    int getRoot() const;
    
    // Liefert die Anzahl der Elemente im Heap.
    size_t size() const;
    
private:
    // Laesst das letzte Element aufsteigen, bis die Heap-Bedingung erfüllt ist.
    void bubbleUp();
    
    // Laesst die Wurzel absinken, bis die Heap-Bedingung erfüllt ist.
    void bubbleDown();
    
    // Liefert die Position des Elternknotens zum angegebenen n.
    size_t parentPos(size_t n);
    
    // Liefert die Position des kleineren Kindknotens zum angegebenen n.
    // Wenn nicht beide Kinder existieren, wird die Position des linken Kindes
    // geliefert.
    size_t childPos(size_t n);
    
    std::vector<int> elements;
};

#endif
