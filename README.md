# wwi19ama-prog2

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Fwwi19ama-prog2/master?urlpath=lab)

Material zum Modul "Programmierung II" im Kurs WWI19AMA an der DHBW Mannheim.

## Links

- [Interaktive Kurs-Umgebung bei mybinder.org](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Fwwi19ama-prog2/master?urlpath=lab)
- [Einzelne Folien-PDFs](https://gitlab.com/reinerh/dhbw/wwi19ama-prog2/-/jobs/artifacts/master/browse?job=deploy:package)
- [Alle Folien als ZIP zum Herunterladen](https://gitlab.com/reinerh/dhbw/wwi19ama-prog2/-/jobs/artifacts/master/download?job=deploy:package)
