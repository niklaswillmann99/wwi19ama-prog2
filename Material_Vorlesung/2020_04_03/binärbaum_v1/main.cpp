#include <iostream>
using namespace std;

// Klasse "Knoten" (ein eigener Datentyp)
struct Knoten
{
  // Daten-Member (Attribute)
  int daten;         // Datensatz

  Knoten * links;    // Linker Teilbaum
  Knoten * rechts;   // Rechter Teilbaum

  // Member-Funktionen ("Methoden" (Vorsicht, etwas eingeschränkter Begriff))
  
  // Konstruktor
  Knoten()
  {
    daten = 0;
    links = nullptr;
    rechts = nullptr;
  }

  // Funktion, die true zurückgibt, wenn der Knoten leer ist.
  bool empty()
  {
    return links == nullptr && rechts == nullptr;
  }

  // Setzt das Datenelement und erzeugt bei Bedarf neue Teilbäume
  void set_data(int x)
  {
    daten = x;

    if (empty())
    {
      links = new Knoten();
      rechts = new Knoten();
    }
  }
};


int main() {
  
  Knoten k;
  cout << k.empty() << endl;   // Es sollte eine 1 ausgegeben werden.

  k.set_data(15);

  cout << k.empty() << endl;   // Es sollte eine 0 ausgegeben werden.
  cout << k.links->empty() << endl;  // 1 erwartet  

  k.links->set_data(3);

  return 0;
}