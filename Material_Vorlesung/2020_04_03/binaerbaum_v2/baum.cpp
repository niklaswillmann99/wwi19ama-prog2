#include "baum.h"

// Implementierung des Konstruktors von Knoten
Knoten::Knoten()
{
  data = 0;
  left = nullptr;
  right = nullptr;
}

// Implementierung der Funktion 'empty' aus der Klasse 'Knoten'.
// Schreibweise: Knoten::empty
bool Knoten::empty()
{
  return left == nullptr && right == nullptr;
}

void Knoten::set_data(int x)
{
  data = x;

  if (empty())
  {
    left = new Knoten();
    right = new Knoten();
  }
}