#ifndef BAUM_H
#define BAUM_H

// Klasse "Knoten" (ein eigener Datentyp)
struct Knoten
{
  // Daten-Member (Attribute)
  int data;         // Datensatz

  Knoten * left;    // Linker Teilbaum
  Knoten * right;   // Rechter Teilbaum

  // Member-Funktionen ("Methoden" (Vorsicht, etwas eingeschränkter Begriff))
  
  // Konstruktor
  Knoten();

  // Funktion, die true zurückgibt, wenn der Knoten leer ist.
  bool empty();

  // Setzt das Datenelement und erzeugt bei Bedarf neue Teilbäume
  void set_data(int x);
  
};

#endif