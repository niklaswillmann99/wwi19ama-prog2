#include <iostream>
using namespace std;

// Elemente einer einfach verketteten Liste
struct Element
{
  int daten = 0;
  Element * next = nullptr;
};

// Die einfach verkettete Liste
struct Liste
{
  Element * head;

  // Konstruktor
  Liste()
  {
    head = new Element(); // Die leere Liste hat ein Dummy-Element
  }

  void push_back(int x)
  {
    // Die Liste nach dem Dummy durchsuchen
    Element * current = head;

    while (current->next != nullptr)
    {
      current = current->next;
    }  // Hier zeigt current auf das Dummy-Element am Ende

    // Neuen Datensatz in den Dummy einsetzen
    current->daten = x;

    // Neuen Dummy anhängen.
    current->next = new Element();
  }

  void print()
  {
    Element * current = head;
    while (current->next != nullptr)
    {
      cout << current->daten << " ";
      current = current->next;
    }
  }
};




int main() {
  Liste l1;

  l1.push_back(42);
  l1.push_back(43);
  l1.push_back(41);
  
  l1.print();
}
