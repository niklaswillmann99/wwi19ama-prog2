# Build-Konfiguration (mittels des Tools "CMake")

cmake_minimum_required(VERSION 3.10)

# Projektname
project(LinkedList-Aufgaben)

# Angabe des C++-Standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS NO)

# Name der Exe-Datei und der zugehörigen Quellen
add_executable(llist linkedlist.cpp aufgaben.cpp test.cpp)
