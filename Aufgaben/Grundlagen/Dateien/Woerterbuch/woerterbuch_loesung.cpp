#include<fstream>
#include<iostream>
#include<map>
#include<string>
#include<sstream>
using namespace std;

/*** AUFGABENSTELLUNG:
 *
 * Schreiben Sie eine Funktion, die einen Dateinamen (string) als Parameter erwartet.
 * Die Funktion soll die angegebene Datei öffnen und die darin stehenden Wörter
 * auslesen. In der Datei stehen immer absechselnd deutsche und englische Wörter.
 *
 * Die Funktion soll dann eine std::map erzeugen, mit den Paaren aus deutschen und
 * englischen Wörtern befüllen und zurückliefern.
 */
map<string,string> read_words(string filename)
{
    map<string,string> result;
    
    // Öffnen Sie hier die Datei 'filename'.
    ifstream file(filename);
    
    // Schreiben Sie eine Schleife, die immer zwei Wörter ausliest
    // und die dieses Wortpaar dann in 'result' einträgt.
    
    string line;
    while(getline(file, line))
    {
      stringstream buffer;
      buffer << line;

      string de;      
      string en;
      if(!(buffer >> de >> en)) 
      {
        return {};  
      }
      result[de] = en;
    }
    

    return result;
}

int main()
{
    map<string,string> dict = read_words("woerter.txt");
    
    string de;
    cout << "Welches Wort soll ich uebersetzen? ";
    cin >> de;
    
    cout << "Die Uebersetzung lautet: " << dict[de] << endl;
    
    return 0;
}