#include<iostream>
#include<string>
#include<vector>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'binfind', die einen aufsteigend sortierten
    vector<int> v sowie eine Zahl x erwartet.
 
    Die Funktion soll die Position eines Vorkommens von x in v bestimmen,
    oder die Länge von v zurückliefern, falls x nicht in v vorkommt.
 
    Die Funktion soll eine binäre Suche durchführen, also in der Mitte
    von v anfangen und dann links oder rechts weitersuchen.
         
    Es bietet sich an, diese Aufgabe rekursiv zu lösen.
 ***/
string binfind(string s);

int main()
{
    vector<int> v1 ={1,3,5,7,9,11,13,15,17,19};
    cout << binfind(v1, 5) << endl;     // soll "2" ausgeben
    cout << binfind(v1, 19) << endl;     // soll "9" ausgeben
    cout << binfind(v1, 42) << endl;     // soll "10" ausgeben

    return 0;
}

