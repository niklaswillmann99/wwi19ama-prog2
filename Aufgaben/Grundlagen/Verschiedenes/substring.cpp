#include<iostream>
#include<string>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'substring', die zwei Strings
    erwartet. Die Funktion soll true zurückliefern, falls der
    erste String im zweiten enthalten ist. Dabei dürfen zwischen
    den Buchstaben aus s1 auch andere Buchstaben in s2 stehen.
 ***/
bool substring(string s1, string s2);

int main()
{
    cout << substring("ab", "abc") << endl;      // Soll "1" ausgeben (true).
    cout << substring("bc", "abc") << endl;      // Soll "1" ausgeben (true).
    cout << substring("ac", "abc") << endl;      // Soll "1" ausgeben (true).
    cout << substring("", "abc") << endl;        // Soll "1" ausgeben (false).
    cout << substring("ae", "abc") << endl;      // Soll "0" ausgeben (false).
    
    return 0;
}

