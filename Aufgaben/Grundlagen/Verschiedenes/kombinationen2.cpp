#include<iostream>
#include<string>
#include<vector>
#include<sstream>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'kombinationen', die einen vector<string> v
    erwartet. Die Funktion soll einen Vector mit allen Kombinationen der
    Wörter in v zurückgeben.
    
    HINWEIS: Es bietet sich an, diese Aufgabe durch Rekursion zu lösen.
 ***/
vector<string> kombinationen(vector<string> v);

/** Hilfsfunktion, gibt einen vector<string> auf der Konsole aus.*/
void print(vector<string> v)
{
    for (string el:v)
    {
        cout << el << " ";
    }
    cout << endl;
}

int main()
{
    vector<string> v1 = kombinationen({"Hallo", "Welt"});
    vector<string> v2 = kombinationen({"Ein", "langer", "Satz"});   
                                                                    
    print(v1);  // Soll "Hallo Welt" und "Welt Hallo" ausgeben.
    
    print(v2);  // Soll "Ein langer Satz", "Ein Satz langer",
                //      "langer Ein Satz", "langer Satz Ein",
                //      "Satz Ein langer" und "Satz langer Ein" ausgeben.
    
    return 0;
}

