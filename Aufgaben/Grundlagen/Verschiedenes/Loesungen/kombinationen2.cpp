#include<iostream>
#include<string>
#include<vector>
#include<sstream>
using namespace std;

/*** AUFGABE (Ein-/Ausgabe):
    Schreiben Sie eine Funktion 'kombinationen', die einen vector<string> v
    erwartet. Die Funktion soll einen Vector mit allen Kombinationen der
    Wörter in v zurückgeben.
    
    HINWEIS: Es bietet sich an, diese Aufgabe durch Rekursion zu lösen.
 ***/
vector<string> kombinationen(vector<string> v);

/** Hilfsfunktion, gibt einen vector<string> auf der Konsole aus.*/
void print(vector<string> v)
{
    for (string el:v)
    {
        cout << el << " ";
    }
    cout << endl;
}

int main()
{
    vector<string> v1 = kombinationen({"Hallo", "Welt"});
    vector<string> v2 = kombinationen({"Ein", "langer", "Satz"});   
                                                                    
    print(v1);  // Soll "Hallo Welt" und "Welt Hallo" ausgeben.
    
    print(v2);  // Soll "Ein langer Satz", "Ein Satz langer",
                //      "langer Ein Satz", "langer Satz Ein",
                //      "Satz Ein langer" und "Satz langer Ein" ausgeben.
    
    return 0;
}

vector<string> kombinationen(vector<string> v)
{
  if (v.empty())
  {
    return {};
  }

  vector<string> result;
  for (int j=0; j<v.size(); j++)
  {
    // v zerlegen in einzelnen String und den Rest der Liste
    string einzeln = v[0];
    v.erase(v.begin());

    // Rekursiver Aufruf: Liefert die Permutationen von Rest-Vector
    auto permutationen = kombinationen(v);

    // Sonderfall: Wenn v leer ist, dann fügen einmal einen leeren String hinzu.
    if (permutationen.empty())
    {
      permutationen.push_back("");
    }

    // Den einzelnen String vor jedes Element hängen
    for (int i=0; i<permutationen.size(); i++)
    {
      permutationen[i] = einzeln + " " + permutationen[i];
    }
    result.insert(result.end(), permutationen.begin(), permutationen.end());

    // Einzelstring wieder zu v hinzufügen
    v.push_back(einzeln);
  }

  return result;
}

