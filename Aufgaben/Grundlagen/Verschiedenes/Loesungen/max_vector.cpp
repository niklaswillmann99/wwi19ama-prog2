#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

/* Aufgabenstellung:
 
   Schreiben Sie eine Funktion max(), die einen Vektor v aus
   Zahlen als Parameter erwartet und das größte Element aus v
   zurückliefert.
 */
int max(vector<int> v)
{
    int result = 0;

    for (auto el:v)
    {
        if (el > result)
        {
            result = el;
        }
    }
    return result;
    
    // Alternative Lösung:
    auto iter = std::max(v.begin(), v.end());
    if (iter != v.end())
    {
        return *iter;
    }
    return 0;
}

int main()
{
    std::cout << max({1,3,5,7,9}) << endl; // Soll 9 ausgeben
    std::cout << max({1,3,2}) << endl;     // Soll 3 ausgeben
    std::cout << max({-42}) << endl;       // Soll -42 ausgeben
    std::cout << max({}) << endl;          // Soll 0 ausgeben
    
    return 0;
}
