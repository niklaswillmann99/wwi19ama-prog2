#include<iostream>
#include<vector>
using namespace std;

/* Aufgabenstellung:
 
   Schreiben Sie eine Funktion summe(), die einen Vektor v aus
   Zahlen als Parameter erwartet und die die Summe aller Elemente
   in v zurückliefert.
 */
int summe(vector<int> v)
{
    int result = 0;
    
    // Schreiben Sie hier eine Schleife, die durch den Vektor
    // läuft und die jedes Element zu result hinzuaddiert.
    
    return result;
}

int main()
{
    std::cout << summe({1,3,5,7,9}) << endl; // Soll 25 ausgeben
    std::cout << summe({1,3}) << endl;       // Soll 4 ausgeben
    std::cout << summe({-42}) << endl;       // Soll -42 ausgeben
    std::cout << summe({}) << endl;          // Soll 0 ausgeben
    
    return 0;
}
