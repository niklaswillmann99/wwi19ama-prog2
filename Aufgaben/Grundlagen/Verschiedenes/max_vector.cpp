#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

/* Aufgabenstellung:
 
   Schreiben Sie eine Funktion max(), die einen Vektor v aus
   Zahlen als Parameter erwartet und das größte Element aus v
   zurückliefert.
 */
int max(vector<int> v)
{
    int result = 0;
    
    // Schreiben Sie hier eine Schleife, die durch den Vektor
    // läuft und dabei jedes Element mit result vergleicht.
    // Wenn ein Element größer ist als result, muss result durch
    // dieses Element ersetzt werden.
    
    // Alternativer Ansatz: Verwenden Sie die Funktion std::max
    
    return result;
}

int main()
{
    std::cout << max({1,3,5,7,9}) << endl; // Soll 9 ausgeben
    std::cout << max({1,3,2}) << endl;     // Soll 3 ausgeben
    std::cout << max({-42}) << endl;       // Soll -42 ausgeben
    std::cout << max({}) << endl;          // Soll 0 ausgeben
    
    return 0;
}
