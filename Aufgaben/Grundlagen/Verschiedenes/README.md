# Grundlagen-Aufgaben

Dieses Verzeichnis enthält verschiedene Grundlagen-Aufgaben, teilweise mit Lösungen.

## Konzepte:

- Kontrollfluss (Schleifen, If-Then,-Else, Funktionen)
- std::vector
- Ein-/Ausgabe mittels `cin` und `cout`.
- Strings
- Rekursion
- geschachtelte Schleifen