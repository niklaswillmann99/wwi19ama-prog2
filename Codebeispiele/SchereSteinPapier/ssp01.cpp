#include<iostream>
#include<string>

/* Erste Einfache Version von Schere-Stein-Papier aus
 * dem Erklärungsnotebook zu Record-Datentypen.
 * (C++Konzepte/05-Structs.ipynb)
 */

// Vorausdeklarationen aller Funktionen und Structs,
// damit die Implementierungsreihenfolge keine Rolle spielt.
// Anmerkung: Structs müssen zuerst kommen, da ihre Größe bekannt sein muss.

// Spieler-Struct mit Membern für Name und Punktestand.
struct Player {
    std::string name;
    int score;
};

// Fragt eine Eingabe vom uebergebenen Spieler ab und liefert
// als Ergebnis 1 (Schere), 2 (Stein) oder 3 (Papier) zurück.
int get_move(Player const &);

// Erzeugt zwei Spieler und führt mehrere Runden durch,
// bis ein Spieler drei Punkte erreicht hat.
void run();

// Main-Funktion, startet das Spiel.
int main()
{
    run();
    
    return 0;
}    

int get_move(Player const & p)
{
    // Spieler zur Eingabe eines Zugs auffordern.
    std::cout << p.name << ", Ihre moeglichen Zuege sind: \n"
              << "1: Schere, 2: Stein, 3: Papier\n\n";
    std::cout << "Bitte geben Sie Ihren Zug ein und bestätigen Sie mit ENTER:" << std::endl;
    
    // Eingabe einlesen und pruefen, wiederholen bis Erfolg.
    bool valid = false;
    std::string input;
    while (!valid)
    {
        std::cin >> input;
        valid = (input == "1" || input == "2" || input == "3");
        if (!valid)
        {
            std::cout << "\nFehler, bitte versuchen Sie es noch einmal:" << std::endl;
        }
    }
    
    // Eingabe in Zahl konvertieren und zurueckliefern.
    // input[0] ist der ASCII-Code von '1', '2' oder '3'.
    // Wir ziehen davon den ASCII-Code von '0' ab,
    // um eine Zahl 1 <= x <= 3 zu erhalten.
    return input[0] - '0';
}

void run()
{
    // Spieler erzeugen.
    Player p1{"Spieler 1", 0};
    Player p2{"Spieler 2", 0};
    
    // Spiel so lange laufen lassen, bis einer 3 Punkte hat.
    while (p1.score < 3 && p2.score < 3)
    {
        // Spielzuege abfragen
        int m1 = get_move(p1);
        int m2 = get_move(p2);
        
        // Auswertung
        if (m1 == m2) // Unentschieden
        {
            std::cout << "Diese Runde endet unentschieden." << std::endl;
        }
        else if (m2 == m1+1 || m2 == m1-2) // Spieler 2 gewinnt.
        {
            std::cout << "Spieler 2 gewinnt diese Runde." << std::endl;
            p2.score++;
        }
        else
        {
            std::cout << "Spieler 1 gewinnt diese Runde." << std::endl;
            p1.score++;
        }
        std::cout << std::endl;
    }
    
    // Ergebnis mitteilen
    std::cout << (p1.score == 3 ? p1.name : p2.name) << " hat gewonnen." << std::endl; 
}