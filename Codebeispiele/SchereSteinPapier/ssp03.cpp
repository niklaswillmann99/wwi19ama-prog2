#include<iostream>
#include<string>

/* Die Version von Schere-Stein-Papier aus
 * dem Erklärungsnotebook zu Klassen und Kapselung.
 * (C++Konzepte/07-Klassen-Sichtbarkeiten.ipynb)
 */

// Vorausdeklarationen aller Funktionen und Structs,
// damit die Implementierungsreihenfolge keine Rolle spielt.
// Anmerkung: Structs müssen zuerst kommen, da ihre Größe bekannt sein muss.

// Spieler-Klasse mit Membern für Name und Punktestand.
class Player {
    std::string name;
    int score;
    
public:
    // Initialisert ein Spieler-Objekt.
    // Sollte genau einmal, direkt nach Konstruktion, aufgerufen werden.
    void init(std::string);
    
    // Liefert den Namen des Spielers.
    std::string get_name();
    
    // Liefert den Punktestand des Spielers.
    int get_score();
    
    // Erhöht den Punktestand des Spielers.
    void increase_score();
    
    // Fragt eine Eingabe vom Spieler ab und liefert
    // als Ergebnis 1 (Schere), 2 (Stein) oder 3 (Papier) zurück.
    int get_move();
};

// Erzeugt zwei Spieler und führt mehrere Runden durch,
// bis ein Spieler drei Punkte erreicht hat.
void run();

// Main-Funktion, startet das Spiel.
int main()
{
    run();
    
    return 0;
}    

void Player::init(std::string name_)
{
    name = name_;
    score = 0;
}

std::string Player::get_name()
{
    return name;
}

int Player::get_score()
{
    return score;
}

void Player::increase_score()
{
    score++;
}

int Player::get_move()
{
    // Spieler zur Eingabe eines Zugs auffordern.
    std::cout << name << ", Ihre moeglichen Zuege sind: \n"
              << "1: Schere, 2: Stein, 3: Papier\n\n";
    std::cout << "Bitte geben Sie Ihren Zug ein und bestätigen Sie mit ENTER:" << std::endl;
    
    // Eingabe einlesen und pruefen, wiederholen bis Erfolg.
    bool valid = false;
    std::string input;
    while (!valid)
    {
        std::cin >> input;
        valid = (input == "1" || input == "2" || input == "3");
        if (!valid)
        {
            std::cout << "\nFehler, bitte versuchen Sie es noch einmal:" << std::endl;
        }
    }
    
    // Eingabe in Zahl konvertieren und zurueckliefern.
    // input[0] ist der ASCII-Code von '1', '2' oder '3'.
    // Wir ziehen davon den ASCII-Code von '0' ab,
    // um eine Zahl 1 <= x <= 3 zu erhalten.
    return input[0] - '0';
}

void run()
{
    // Spieler erzeugen.
    Player p1, p2;
    p1.init("Spieler 1");
    p2.init("Spieler 2");    
    
    // Spiel so lange laufen lassen, bis einer 3 Punkte hat.
    while (p1.get_score() < 3 && p2.get_score() < 3)
    {
        // Spielzuege abfragen
        int m1 = p1.get_move();
        int m2 = p2.get_move();
        
        // Auswertung
        if (m1 == m2) // Unentschieden
        {
            std::cout << "Diese Runde endet unentschieden." << std::endl;
        }
        else if (m2 == m1+1 || m2 == m1-2) // Spieler 2 gewinnt.
        {
            std::cout << p2.get_name() << " gewinnt diese Runde." << std::endl;
            p2.increase_score();
        }
        else
        {
            std::cout << p1.get_name() << " gewinnt diese Runde." << std::endl;
            p1.increase_score();
        }
        std::cout << std::endl;
    }
    
    // Ergebnis mitteilen
    std::cout << (p1.get_score() == 3 ? p1.get_name() : p2.get_name()) << " hat gewonnen." << std::endl;
}