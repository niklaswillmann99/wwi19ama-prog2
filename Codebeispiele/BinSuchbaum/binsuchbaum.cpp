#include"binsuchbaum.h"
#include"knoten.h"

#include<sstream>
using namespace std;

BinSuchBaum::BinSuchBaum()
: root{new Knoten} {}

bool BinSuchBaum::empty()
{
	return root->empty();
}

void BinSuchBaum::add(int newdata)
{
	root->add(newdata);
}

string BinSuchBaum::str()
{
	if (root->empty()) return " ";
	
    // In-Order-Durchlauf durch den Baum
    // Rekursiv: Erst linker Teilbaum, dann Wurzel, dann rechter Teilbaum.
	stringstream s;
	s << BinSuchBaum(root->lchild).str()
	  << *root
	  << BinSuchBaum(root->rchild).str();
	return s.str();
    
    /* Anmerkung:
     * BinSuchBaum(root->lchild).str() konstruiert on-the-fly einen binären Suchbaum
     * aus dem Knoten root->lchild. Auf diesem wird dann die str()-Funktion aufgerufen.
     */
}

ostream & operator<<(ostream & left, BinSuchBaum & right)
{
	return left << right.str();
}

BinSuchBaum::BinSuchBaum(Knoten * k)
: root{k} {}
