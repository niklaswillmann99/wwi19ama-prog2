#include "knoten.h"
using namespace std;

bool Knoten::empty()
{
	return lchild == nullptr && rchild == nullptr;
}
	
void Knoten::add(int newdata)
{
	if (empty())
	{
		data = newdata;
		lchild = new Knoten;
		rchild = new Knoten;
		return;
	}
	if (newdata < data) lchild->add(newdata);
	else rchild->add(newdata);	
}

string Knoten::str()
{
	return std::to_string(data);
}

ostream & operator<<(ostream & left, Knoten & right)
{
	return left << right.str();
}
