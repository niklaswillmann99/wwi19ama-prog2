#ifndef KNOTEN_H
#define KNOTEN_H

#include<iostream>
#include<string>

class Knoten
{
	int data;					// Daten dieses Knotens
	Knoten * lchild = nullptr;	// linkes Kind
	Knoten * rchild = nullptr;  // rechtes Kind
	/* Anmerkung: Ein frischer Knoten ist erstmal ein Dummy-Knoten.*/

public:
	/** Gibt an, ob der Baum leer ist. **/
	bool empty();
	
	/** Fuegt ein neues Element hinzu.**/
	void add(int newdata);
	
	/** Gibt die Daten dieses Knotens als String zurueck.**/
	std::string str();
	
	/** Liefert einen Pointer auf einen Knoten
	    mit dem angegebenen Schluessel.**/
	Knoten * find(int requestedKey);
	
	/** Legt fest, dass die Klasse 'BinSuchBaum' auf die
	    privaten Member von 'Knoten' zugreifen darf.**/
	friend class BinSuchBaum;
};

/** Ueberladung des Stream-Ausgabe-Operators **/
std::ostream & operator<<(std::ostream & left, Knoten & right);



#endif
