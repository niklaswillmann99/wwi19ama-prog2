#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std; 

template<typename T>
void move_left(
  typename vector<T>::iterator begin,
  typename vector<T>::iterator end
)
{
  for (
    auto current = end;
    current > begin && *current < *(current-1);
    current--)
  {
    swap(*current, *(current-1));
  }
}

// Template-Sortier-Funktion
// Funktioniert mit jedem Vektor, dessen Elemente
// mit < verglichen werden können.
template<typename T>
void insertsort(
  typename vector<T>::iterator begin,
  typename vector<T>::iterator end
)
{
  for (auto current = begin; current < end; current++)
  {
    move_left<T>(begin, current);
  }
}

// Vorausdeklaration von print
template<typename T>
void print(
  typename vector<T>::iterator begin,
  typename vector<T>::iterator end);

int main() {

  vector<int> v1{3, 15, 42, 8, 38};
  vector<string> v2{"ist", "das", "hier", "sortiert"};
  
  insertsort<int>(v1.begin(), v1.end());
  insertsort<string>(v2.begin(), v2.end());

  print<int>(v1.begin(), v1.end());
  print<string>(v2.begin(), v2.end());

  return 0;
}

// Template-Print-Funktion.
// Funktioniert mit jedem Vektor, dessen Elemente
// per cout auf die Konsole gegeben werden können.
template<typename T>
void print(
  typename vector<T>::iterator begin,
  typename vector<T>::iterator end)
{
  for (auto it=begin; it<end; it++)
  {
    cout << *it << " ";
  }
  cout << endl;
}

