#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std; 

void move_left(
  vector<int>::iterator begin,
  vector<int>::iterator end
)
{
  for (
    auto current = end;
    current > begin && *current < *(current-1);
    current--)
  {
    swap(*current, *(current-1));
  }
}

void insertsort(
  vector<int>::iterator begin,
  vector<int>::iterator end
)
{
  for (auto current = begin; current < end; current++)
  {
    move_left(begin, current);
  }
}

// Vorausdeklaration von print
void print(
  vector<int>::iterator begin,
  vector<int>::iterator end);

int main() {

  vector<int> v1{3, 15, 42, 8, 38};
  
  insertsort(v1.begin(), v1.end());

  print(v1.begin(), v1.end());

  return 0;
}

void print(
  vector<int>::iterator begin,
  vector<int>::iterator end)
{
  for (auto it=begin; it<end; it++)
  {
    cout << *it << " ";
  }
  cout << endl;
}
